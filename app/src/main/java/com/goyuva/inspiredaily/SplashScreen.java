package com.goyuva.inspiredaily;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.goyuva.inspiredaily.DBops.DBhelper;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SplashScreen extends AppCompatActivity {

    private static final String TAG = "TestQuotes";
    @Bind(R.id.tv_appname)
    TextView tvAppname;
    @Bind(R.id.tv_app_motto)
    TextView tvAppMotto;
    @Bind(R.id.tv_app_logo)
    ImageView tvAppLogo;
    @Bind(R.id.llTitle)
    LinearLayout llTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_right_out);
        try {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        } catch (Exception e) {
            e.printStackTrace();
        }

        setContentView(R.layout.activity_spalsh);
        ButterKnife.bind(this);
        DBhelper dBhelper = DBhelper.getInstance(SplashScreen.this);
        DBhelper daDBhelper1 = new DBhelper(SplashScreen.this);
        //  daDBhelper1.AddSomeQuotes(daDBhelper1.getWritableDatabase());

        //SQLiteDatabase sqdata = dBhelper.getReadableDatabase();
        //  Log.d(TAG, "DAtabase path" + sqdata.getPath());

        tvAppMotto.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in));
        tvAppname.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in));
        llTitle.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in));
        int SPLASH_TIME_OUT = 5000;
        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {

                // This method will be executed once the timer is over
                // Start your app main activity
                Intent i = new Intent(SplashScreen.this, HomeActivity.class);
                startActivity(i);

                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        overridePendingTransition(R.anim.push_left_in, R.anim.push_right_out);
    }
}
