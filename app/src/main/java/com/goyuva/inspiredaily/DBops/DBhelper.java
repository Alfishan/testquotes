package com.goyuva.inspiredaily.DBops;

import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


/**
 * Created by root on 8/4/16.
 */
public class DBhelper extends SQLiteOpenHelper {


    public static final String[] quotes = {"When all is said and done, more is said than done.",
            "When someone tells me \"no,\" it doesn't mean I can't do it, it simply means I can't do it with them.",
            "The only thing standing between you and your goal is the bullshit story you keep telling yourself as to why you can't achieve it.",
            "Of course motivation is not permanent. But then, neither is bathing; but it is something you should do on a regular basis."};
    public static final String[] quotes_author = {"Lou Holtz", "Karen E. Quinones Miller", "Jordan Belfort", "Zig Ziglar"};
    public static final String KEY_QUOTE_TEXT = "quote_text";
    public static final String KEY_QUOTE_AUTHOR = "quote_author";
    public static final String KEY_IS_FEV = "is_fev";
    private static final String TAG = "TestQuotes";
    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "DB_Quotes";
    // Contacts table name
    private static final String TABLE_QUOTE_TEST = "Quotes_test";
    //--------------Queries
    public static final String getAllQuotes = "select * from " + TABLE_QUOTE_TEST;
    private static final String KEY_ID = "id";
    private static String DB_PATH = "/data/data/com.goyuva.inspiredaily/databases/";
    private static DBhelper mInstance = null;
    private Context mCxt;

    /**
     * constructor should be private to prevent direct instantiation.
     * make call to static factory method "getInstance()" instead.
     */
    public DBhelper(Context m1ctx) {
        super(m1ctx, DATABASE_NAME, null, DATABASE_VERSION);
        this.mCxt = m1ctx;
    }

    public static DBhelper getInstance(Context ctx) {
        /**
         * use the application context as suggested by CommonsWare.
         * this will ensure that you dont accidentally leak an Activitys
         * context (see this article for more information:
         * http://developer.android.com/resources/articles/avoiding-memory-leaks.html)
         */
        if (mInstance == null) {
            mInstance = new DBhelper(ctx.getApplicationContext());
        }
        return mInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {


        String Q_CREATE_TABLE_Quotes_test = "create TABLE IF NOT EXISTS " + TABLE_QUOTE_TEST + " (" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + KEY_QUOTE_TEXT + " text, " + KEY_QUOTE_AUTHOR + " text, " + KEY_IS_FEV + " INTEGER" + ")";
        Log.d(TAG, "Query : " + Q_CREATE_TABLE_Quotes_test);
        db.execSQL(Q_CREATE_TABLE_Quotes_test);

        File database = mCxt.getDatabasePath("quotes.db");
        // boolean dbExist = checkDataBase();

        if (database.exists()) {
            //do nothing - database already exist
        } else {

            //By calling this method and empty database will be created into the default system path
            //of your application so we are gonna be able to overwrite that database with our database.
            //  this.getReadableDatabase();

            try {

                copyDatabase(database);
                //copyDataBase();
                File dbexists = mCxt.getDatabasePath("quotes.db");
                Log.d(TAG, "Database copied:+" + dbexists.exists());


            } catch (IOException e) {

                throw new Error("Error copying database");

            }
        }
        AddSomeQuotes(db);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }





    public void AddSomeQuotes(SQLiteDatabase db) {

        try {
            for (int i = 0; i <= quotes.length - 1; i++) {
                ContentValues values = new ContentValues();
                values.put(KEY_QUOTE_TEXT, quotes[i]);
                values.put(KEY_QUOTE_AUTHOR, quotes_author[i]);
                values.put(KEY_IS_FEV, 1);
                long result = db.insert(TABLE_QUOTE_TEST, null, values);
              //  Log.d(TAG, "AddSomeQuotes:" + result);
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
        }


    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public Cursor GetAllQuotes() {
        Cursor cursor = null;
        File database = mCxt.getDatabasePath("quotes.db");
        SQLiteDatabase db = this.getWritableDatabase();

        // boolean dbExist = checkDataBase();

        if (database.exists()) {
            SQLiteDatabase db1 = SQLiteDatabase.openDatabase(DB_PATH + "quotes.db", null, SQLiteDatabase.OPEN_READONLY);
            cursor = db1.rawQuery(getAllQuotes, null, null);
            //do nothing - database already exist
            if (cursor != null)
                cursor.moveToFirst();
          //  Log.d(TAG, "GetAllQuotes: c counts " + cursor.getCount());
            db1.close();
        } else {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                cursor = db.rawQuery(getAllQuotes, null, null);
            }
            if (cursor != null)
                cursor.moveToFirst();

          //  Log.d(TAG, "GetAllQuotes: c counts " + cursor.getCount());
        }

        return cursor;

    }

    private void copyDatabase(File dbFile) throws IOException {
        InputStream is = mCxt.getAssets().open("quotes.db");
        OutputStream os = new FileOutputStream(dbFile);

        byte[] buffer = new byte[1024];
        while (is.read(buffer) > 0) {
            os.write(buffer);
        }

        os.flush();
        os.close();
        is.close();
    }


}
