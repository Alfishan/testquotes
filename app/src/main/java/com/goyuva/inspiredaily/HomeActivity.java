package com.goyuva.inspiredaily;

import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.content.ClipData;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.ClipboardManager;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.goyuva.inspiredaily.DBops.DBhelper;

import java.util.Calendar;
import java.util.Random;

import butterknife.Bind;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity implements GestureDetector.OnGestureListener, View.OnClickListener {


    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_MAX_OFF_PATH = 250;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;
    private static final String TAG = "TestQuotes";
    public static int cursorpos;
    static int min = 1;
    static int max;
    public ObjectAnimator anim1;
    public ObjectAnimator anim2;
    int i1;
    @Bind(R.id.ll_btn_left)
    LinearLayout llBtnLeft;
    @Bind(R.id.ll_btn_right)
    LinearLayout llBtnRight;
    @Bind(R.id.ll_btn_copy)
    LinearLayout llBtnCopy;
    @Bind(R.id.tv_quotes_text)
    TextView tvQuotesText;
    @Bind(R.id.tv_quotes_auther)
    TextView tvQuotesAuther;
    @Bind(R.id.linearLayout)
    LinearLayout linearLayout;
    @Bind(R.id.tv_status)
    TextView tvStatus;
    @Bind(R.id.fab_btn_share)
    FloatingActionButton fabBtnShare;
    Cursor cursor_All_Quotes;
    DBhelper mDBhelper;
    SharedPreferences mSharedPreferences;
    SharedPreferences.Editor mEditor;
    Random r = new Random();
    int qoute_id;
    int date;
    boolean isset;
    AdRequest adRequest;
    AdView adView;
    @Bind(R.id.scrollview1)
    ScrollView scrollview1;
    private GestureDetectorCompat gDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        try {
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        } catch (Exception e) {
            e.printStackTrace();
        }

        overridePendingTransition(R.anim.push_left_in, R.anim.push_right_out);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        assert toolbar != null;
        toolbar.setNavigationIcon(R.mipmap.ic_toolbat_logo);
        mDBhelper = DBhelper.getInstance(HomeActivity.this);
        init();


    }

    private void QoD() {
        mSharedPreferences = getSharedPreferences(Const_Strings.constant_Shared_pref_QoD_name, HomeActivity.MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();
        Calendar mCalendar = Calendar.getInstance();
        isset = mSharedPreferences.getBoolean(Const_Strings.constant_Shared_pref_KEY_QOD_qoute_is_set, false);
        date = mSharedPreferences.getInt(Const_Strings.constant_Shared_pref_KEY_QOD_date, 999);
        qoute_id = mSharedPreferences.getInt(Const_Strings.constant_Shared_pref_KEY_QOD_qoute_id, 989830);
        Log.d(TAG, "QOD: isset: " + isset + "  data:" + date + "qoute id: " + qoute_id + " rand num:" + i1);

        if (!isset) {

            mEditor.putInt(Const_Strings.constant_Shared_pref_KEY_QOD_date, mCalendar.get(Calendar.DAY_OF_YEAR));
            mEditor.putInt(Const_Strings.constant_Shared_pref_KEY_QOD_qoute_id, i1);
            mEditor.putBoolean(Const_Strings.constant_Shared_pref_KEY_QOD_qoute_is_set, true);


        }
        if (mCalendar.get(Calendar.DAY_OF_YEAR) != date && isset) {

            mEditor.putInt(Const_Strings.constant_Shared_pref_KEY_QOD_qoute_id, i1);
            mEditor.putInt(Const_Strings.constant_Shared_pref_KEY_QOD_date, mCalendar.get(Calendar.DAY_OF_YEAR));
            mEditor.putBoolean(Const_Strings.constant_Shared_pref_KEY_QOD_qoute_is_set, true);

        }

        isset = mSharedPreferences.getBoolean(Const_Strings.constant_Shared_pref_KEY_QOD_qoute_is_set, false);
        date = mSharedPreferences.getInt(Const_Strings.constant_Shared_pref_KEY_QOD_date, 999);
        qoute_id = mSharedPreferences.getInt(Const_Strings.constant_Shared_pref_KEY_QOD_qoute_id, 989830);
        mEditor.apply();
        moveQuotes(5);
        Log.d(TAG, "QOD: isset: " + isset + "  data:" + date + "qoute id: " + qoute_id + " rand num:" + i1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_setting) {

            Intent intent_setting = new Intent(HomeActivity.this, Setting_Activity.class);
            startActivity(intent_setting);
          //  Toast.makeText(HomeActivity.this, "Settings", Toast.LENGTH_SHORT).show();

        }

        return false;
    }

    private void init() {

        cursor_All_Quotes = mDBhelper.GetAllQuotes();
        max = cursor_All_Quotes.getCount() - 1;
        i1 = r.nextInt(max - min + 1) + min;
        gDetector = new GestureDetectorCompat(HomeActivity.this, this);
        llBtnLeft.setOnClickListener(this);
        llBtnRight.setOnClickListener(this);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf");
        tvQuotesText.setTypeface(font);
        tvQuotesAuther.setTypeface(font);
        llBtnCopy.setOnClickListener(this);
        fabBtnShare.setOnClickListener(this);
        // Ads thing

        adView = (AdView) this.findViewById(R.id.adView_home);
        adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);


        scrollview1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gDetector.onTouchEvent(event);

            }
        });

        initanim();
        QoD();
        //   moveQuotes(1);


    }

    private void initanim() {

        anim1 = (ObjectAnimator) AnimatorInflater.loadAnimator(getApplicationContext(), R.animator.anim_obj);
        anim1.setTarget(tvQuotesAuther);
        anim1.setDuration(200);
        anim2 = (ObjectAnimator) AnimatorInflater.loadAnimator(getApplicationContext(), R.animator.anim_obj);
        anim2.setTarget(tvQuotesText);
        anim2.setDuration(200);
    }


    @Override
    public void onClick(View v) {

        int id = v.getId();

        if (id == R.id.fab_btn_share) {

            fabBtnShare.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in));
             Toast.makeText(HomeActivity.this, "share", Toast.LENGTH_SHORT).show();
            ShareQuote(" \" " + tvQuotesText.getText() + " \" " + "-" + tvQuotesAuther.getText());
        }

        if (id == R.id.ll_btn_copy) {

            llBtnCopy.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in));

            int sdk = Build.VERSION.SDK_INT;
            if (sdk < Build.VERSION_CODES.HONEYCOMB) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(HomeActivity.CLIPBOARD_SERVICE);
                clipboard.setText(" \" " + tvQuotesText.getText() + " \" " + "-" + tvQuotesAuther.getText());
            } else {
                android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(HomeActivity.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Quote from TestQuoteApp", " \" " + tvQuotesText.getText() + " \" " + "-" + tvQuotesAuther.getText());
                clipboard.setPrimaryClip(clip);
            }
            customToest("sdas");

        }

        if (id == R.id.ll_btn_left) {


            moveQuotes(4);
            llBtnLeft.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.push_in));

        }
        if (id == R.id.ll_btn_right) {

            moveQuotes(3);
            llBtnRight.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.push_out));

        }



    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        this.gDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent e) {
        //Toast.makeText(getApplicationContext(), "OnDown Gesture", Toast.LENGTH_SHORT).show();
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        try {
            if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
                return false;
            // right to left swipe
            if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {

                moveQuotes(3);
                //Toast.makeText(HomeActivity.this, "Left Swipe", Toast.LENGTH_SHORT).show();
            } else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                moveQuotes(4);
                //Toast.makeText(HomeActivity.this, "Right Swipe", Toast.LENGTH_SHORT).show();

            }
        } catch (Exception e) {
            // nothing
        }
        return false;
    }


    public void ShareQuote(String quoteTExt) {

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, quoteTExt);
        sendIntent.setType("text/plain");
        startActivityForResult(Intent.createChooser(sendIntent, "Share"), 1234);

    }


    @Override
    public void onPause() {
        overridePendingTransition(R.anim.push_left_in, R.anim.push_right_out);
        if (adView != null) {
            adView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (adView != null) {
            adView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (adView != null) {
            adView.destroy();
        }
        super.onDestroy();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        overridePendingTransition(R.anim.push_left_in, R.anim.push_right_out);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

       /* if (resultCode == Activity.RESULT_CANCELED && resultCode != Activity.RESULT_OK) {
            customToest("sad");
            //  Toast.makeText(HomeActivity.this, "oh why", Toast.LENGTH_SHORT).show();
            return;
        }*/

        switch (requestCode) {
            case 1234:
                customToest("happy");
                break;
        }
    }


    public void moveQuotes(int oppcode) {


        switch (oppcode) {

            case 1:


                if (cursor_All_Quotes != null) {
                    cursor_All_Quotes.moveToFirst();
                    int q_a_index = cursor_All_Quotes.getColumnIndex("quote_author");
                    int q_t_index = cursor_All_Quotes.getColumnIndex("quote_text");

                    setTexts(cursor_All_Quotes.getString(q_t_index), cursor_All_Quotes.getString(q_a_index));
                 //   Log.d(TAG, "cursorpos:" + cursor_All_Quotes.getPosition());
                    countStatus();
                }

                break;
            case 2:

                if (cursor_All_Quotes != null) {

                    cursor_All_Quotes.moveToLast();
                    cursorpos = cursor_All_Quotes.getPosition();;
                    int q_a_index = cursor_All_Quotes.getColumnIndex("quote_author");
                    int q_t_index = cursor_All_Quotes.getColumnIndex("quote_text");
                    setTexts(cursor_All_Quotes.getString(q_t_index), cursor_All_Quotes.getString(q_a_index));
                    countStatus();
                  //  Log.d(TAG, "cursorpos:" + cursor_All_Quotes.getPosition());
                }

                break;
            case 3:

                if (cursor_All_Quotes != null) {

                    if (cursor_All_Quotes.getPosition() < cursor_All_Quotes.getCount() - 1) {

                        try {
                            //  cursor_All_Quotes.moveToPosition(++cursorpos);
                            //cursorpos = cursor_All_Quotes.getPosition();
                            cursor_All_Quotes.moveToNext();
                            int q_a_index = cursor_All_Quotes.getColumnIndex("quote_author");
                            int q_t_index = cursor_All_Quotes.getColumnIndex("quote_text");
                            setTexts(cursor_All_Quotes.getString(q_t_index), cursor_All_Quotes.getString(q_a_index));
                            countStatus();
                          //  Log.d(TAG, "cursorpos:" + cursor_All_Quotes.getPosition());
                        } catch (CursorIndexOutOfBoundsException e) {

                            e.printStackTrace();
                        }


                    } else {
                        Toast.makeText(HomeActivity.this, "You are Already At Last Quote :) ", Toast.LENGTH_SHORT).show();

                    }
                }

                break;

            case 4:

                if (cursor_All_Quotes != null) {

                    if (cursor_All_Quotes.getPosition() > 0) {

                        try {
                           /* cursor_All_Quotes.moveToPosition(--cursorpos);
                            cursorpos = cursor_All_Quotes.getPosition();*/
                            cursor_All_Quotes.moveToPrevious();
                            int q_a_index = cursor_All_Quotes.getColumnIndex("quote_author");
                            int q_t_index = cursor_All_Quotes.getColumnIndex("quote_text");
                            countStatus();
                            setTexts(cursor_All_Quotes.getString(q_t_index), cursor_All_Quotes.getString(q_a_index));
                          //  Log.d(TAG, "cursorpos:" + cursor_All_Quotes.getPosition());
                        } catch (CursorIndexOutOfBoundsException e) {

                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(HomeActivity.this, "You are Already At first Quote :) ", Toast.LENGTH_SHORT).show();

                    }
                }
                break;

            case 5:

                if (cursor_All_Quotes != null) {


                    try {
                           /* cursor_All_Quotes.moveToPosition(--cursorpos);
                            cursorpos = cursor_All_Quotes.getPosition();*/
                        if (qoute_id < cursor_All_Quotes.getCount()) {

                            cursor_All_Quotes.moveToPosition(qoute_id);
                        } else {
                            cursor_All_Quotes.moveToPosition(i1);

                        }
                        int q_a_index = cursor_All_Quotes.getColumnIndex("quote_author");
                        int q_t_index = cursor_All_Quotes.getColumnIndex("quote_text");
                        countStatus();
                        setTexts(cursor_All_Quotes.getString(q_t_index), cursor_All_Quotes.getString(q_a_index));
                      //  Log.d(TAG, "cursorpos:" + cursor_All_Quotes.getPosition());
                    } catch (CursorIndexOutOfBoundsException e) {

                        e.printStackTrace();
                    }

                }


                break;
        }


    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void setTexts(String Q_text, String A_text) {

        //  tvQuotesText.startAnimation(AnimationUtils.loadAnimation(HomeActivity.this, R.anim.push_left_in));
        // tvQuotesAuther.startAnimation(AnimationUtils.loadAnimation(HomeActivity.this, R.anim.push_right_in));
        tvQuotesAuther.setText(A_text);
        tvQuotesText.setText(Q_text);
        anim1.start();
        anim2.start();


    }


    public void customToest(String oppcode) {


        //Creating the LayoutInflater instance
        LayoutInflater li = getLayoutInflater();
        //Getting the View object as defined in the customtoast.xml file
        View layout = li.inflate(R.layout.custometoest, (ViewGroup) findViewById(R.id.custom_toast_layout));
        //Creating the Toast object
        ImageView iv = (ImageView) layout.findViewById(R.id.ct_iv);
        TextView tv = (TextView) layout.findViewById(R.id.ct_tv_text);
        tv.setTypeface(Typeface.SANS_SERIF);
        iv.startAnimation(AnimationUtils.loadAnimation(HomeActivity.this, R.anim.push_left_in));
        tv.startAnimation(AnimationUtils.loadAnimation(HomeActivity.this, R.anim.push_right_in));
        switch (oppcode) {
            case "happy":
                iv.setImageDrawable(getResources().getDrawable(R.drawable.grinning_face));
                tv.setText(Const_Strings.constant_ct_happy);
                break;
            default:
                iv.setImageDrawable(getResources().getDrawable(R.drawable.emoji_thumbs_up));
                tv.setText(Const_Strings.constant_ct_copy);
                break;
        }

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);//setting the view of custom toast layout
        toast.show();


    }

    public void countStatus() {
        String Stat1 = cursor_All_Quotes.getPosition() + 1 + "/" + (cursor_All_Quotes.getCount());

        tvStatus.setText(Stat1);
        if (cursor_All_Quotes.getPosition() != cursor_All_Quotes.getColumnCount()) {
        }
    }


}



