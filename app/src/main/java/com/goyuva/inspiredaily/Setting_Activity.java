package com.goyuva.inspiredaily;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;

public class Setting_Activity extends AppCompatActivity implements View.OnClickListener {

    @Bind(R.id.tv_about)
    TextView tvAbout;
    Typeface font;
    @Bind(R.id.ll_about)
    LinearLayout llAbout;
    @Bind(R.id.tv_share)
    TextView tvShare;
    @Bind(R.id.ll_share)
    LinearLayout llShare;
    ImageView iv_fb;
    ImageView iv_twitter;
    ImageView iv_goyuva;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_right_out);

        setContentView(R.layout.activity_setting);
        try {
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        } catch (Exception e) {
            e.printStackTrace();
        }
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();

    }

    private void init() {
        font = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf");
        tvAbout.setTypeface(font);
        tvAbout.setOnClickListener(this);
        llAbout.setOnClickListener(this);
        tvShare.setTypeface(font);
        tvShare.setOnClickListener(this);
        llShare.setOnClickListener(this);


        //About dialog


    }

    @Override
    protected void onRestart() {
        super.onRestart();
        overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    protected void showAbout() {
        // Inflate the about message contents


        // When linking text, force to always use default color. This works
        // around a pressed color state bug.
      /*  TextView textView = (TextView) messageView.findViewById(R.id.about_credits);
        int defaultColor = textView.getTextColors().getDefaultColor();
        textView.setTextColor(defaultColor);
        textView.setTypeface(font);*/
        // textView.startAnimation(AnimationUtils.loadAnimation(Setting_Activity.this, R.anim.fade_in));

        View messageView = getLayoutInflater().inflate(R.layout.about, null, false);
        iv_fb = (ImageView) messageView.findViewById(R.id.iv_about_socialbtn_fb);
        iv_twitter = (ImageView) messageView.findViewById(R.id.iv_about_socialbtn_twitter);


        iv_fb.setOnClickListener(this);
        iv_twitter.setOnClickListener(this);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setTitle(R.string.app_name);
        builder.setView(messageView);
        builder.create();
        builder.show();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.ll_about) {

            showAbout();
        }
        if (v.getId() == R.id.tv_about) {

            showAbout();
        }
        if (v.getId() == R.id.ll_share) {

            share();
        }
        if (v.getId() == R.id.tv_share) {

            share();
        }

        if (v.getId() == R.id.iv_about_socialbtn_fb) {

            social(this.getApplicationContext().getString(R.string.scocial_fb));

        }
        if (v.getId() == R.id.iv_about_socialbtn_twitter) {
            social(this.getApplicationContext().getString(R.string.scocial_twitter));

        }

    }

    private void share() {

        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        //shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, "Hey check out my app at: http://www.goyuva.com/");
        startActivityForResult(Intent.createChooser(shareIntent, "Share"), 92);

    }

    private void social(String url) {

        try {
            startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse(url)));
        } catch (Exception e) {

            Toast.makeText(Setting_Activity.this, "Sorry something went wrong!!!", Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        switch (requestCode) {
            case 92:
                customToest("happy");
                break;
        }
    }

    public void customToest(String oppcode) {


        //Creating the LayoutInflater instance
        LayoutInflater li = getLayoutInflater();
        //Getting the View object as defined in the customtoast.xml file
        View layout = li.inflate(R.layout.custometoest, (ViewGroup) findViewById(R.id.custom_toast_layout));
        //Creating the Toast object
        ImageView iv = (ImageView) layout.findViewById(R.id.ct_iv);
        TextView tv = (TextView) layout.findViewById(R.id.ct_tv_text);
        iv.startAnimation(AnimationUtils.loadAnimation(Setting_Activity.this, R.anim.push_left_in));
        tv.startAnimation(AnimationUtils.loadAnimation(Setting_Activity.this, R.anim.push_right_in));
        switch (oppcode) {
            case "happy":
                iv.setImageDrawable(getResources().getDrawable(R.drawable.grinning_face));
                tv.setText(Const_Strings.constant_ct_happy);
                break;
            default:
                iv.setImageDrawable(getResources().getDrawable(R.drawable.emoji_thumbs_up));
                tv.setText(Const_Strings.constant_ct_copy);
                break;
        }

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);//setting the view of custom toast layout
        toast.show();


    }
}
